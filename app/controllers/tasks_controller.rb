class TasksController < ApplicationController
  before_action :authenticate_user!
  before_action :find_task, only: [:show, :edit, :update, :destroy, :share]
  before_action :get_tasks, only: [:index, :create]

  def index; end

  def show; end

  def new
    @task = Task.new
  end

  def edit; end

  def create
    @task = current_user.tasks.build(task_params)

    if @task.save
      sync_new @task, scope: @tasks
      redirect_to @task, notice: 'Task was successfully created.'
    else
      render :new, notice: 'Task was already shared with this user.'
    end
  end

  def update
    if @task.update(task_params)
      sync_update @task
      redirect_to @task, notice: 'Task was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @task.destroy
    sync_destroy @task
    redirect_to tasks_url, notice: 'Task was successfully destroyed.'
  end

  def share; end

  def create_shared
    user = User.find_by_email(params[:create_shared][:email])
    
    if user.nil? || SharedTask.where(user_id: user_id, task_id: params[:id]).any?
      flash[:notice] = "Task was already shared with this user or User with this email doesn't exist."
      render :share
    else
      SharedTask.create(user_id: user.id, task_id: params[:id])
      redirect_to tasks_url, notice: 'Task was successfully shared.'
    end
  end

  private
    def find_task
      @task = Task.find(params[:id])
    end

    def task_params
      params.require(:task).permit(:name, :description)
    end

    def get_tasks
      user_id = current_user.id
      task_ids = SharedTask.where(user_id: user_id).map(&:task_id)
      tasks = Task.where(user_id: user_id)
      shared_tasks = Task.where(id: task_ids)
      @tasks = tasks + shared_tasks
    end

    def find_user(params)
      if User.find_by_email(params[:create_shared][:email]).id
        user_id =  User.find_by_email(params[:create_shared][:email]).id
      else
        flash[:notice] = "User with this email don't exist."
        render :share
      end
    end
end
