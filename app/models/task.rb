class Task < ActiveRecord::Base
  belongs_to :user
  has_many :sharedTasks, dependent: :destroy

  validates :name, presence: true
  validates :description, presence: true

  def owner?(current_user)
    self.user_id.eql?(current_user.id)
  end
end
