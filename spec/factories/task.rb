FactoryGirl.define do
  factory :task do
    sequence(:name) { |n| "task_#{n}" }
    sequence(:description) { |n| "some description_#{n}" }
  end

  factory :invalid_task, parent: :task do
  	name nil
  end
end