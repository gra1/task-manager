require "rails_helper"

RSpec.describe Task, :type => :model do
	context "checks association" do
		it { should have_many(:sharedTasks).dependent(:destroy) }
		it { should belong_to(:user) }
	end

	context "checking validations" do
		it { should validate_presence_of(:name) }
		it { should validate_presence_of(:description) }
	end

	context 'check owner' do
		before(:each) do
			@owner_user = create(:user)
			@user = create(:user)
			@task = create(:task, user_id: @owner_user.id)
		end

		it 'is true' do
			expect(@task.owner?(@owner_user)).to eq(true)
		end

		it 'is false' do
			expect(@task.owner?(@user)).to eq(false)
		end
	end
end