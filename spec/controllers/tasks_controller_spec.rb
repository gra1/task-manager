require "rails_helper"

RSpec.describe TasksController, :type => :controller do
	before(:each) do
		@user = create(:user)
		@task = create(:task, user_id: @user.id)
		login_with @user
	end

	context 'GET index' do
		it "assigns tasks" do      
      task2 = create(:task, user_id: create(:user).id)
      shared_task = SharedTask.create(user_id: @user.id, task_id: task2.id)
      shared_task = Task.where(user_id: shared_task.task_id).first
      get :index
      expect(assigns(:tasks)).to eq([@task, shared_task])
    end

		it "renders the index template" do			
      get :index
      expect(response).to render_template("index")
    end

    it "has a 200 status" do
      get :index
      expect(response.status).to eq(200)
    end
	end

	context 'GET show' do
		it 'assigns task' do
			get :show, id: @task
			expect(assigns(:task)).to eq(@task)
		end

		it 'render show page' do
			get :show, id: @task
			expect(response).to render_template(:show)
		end
	end

	context "GET new" do
		context "valid data" do
			it 'task should be a new Task' do
				get :new, id: create(:task)
				expect(assigns(:task)).to be_a_new(Task)
			end

			it 'expected response from new page' do
				get :new, id: create(:task)
				expect(response).to render_template(:new)
			end
		end
		
		context "invalid data" do
			it "task should not be a new Task" do
				get :new
				expect(assigns(:tasks)).to_not be_a_new(Task)
			end

			it "unexpected response from new page" do
				get :new
				expect(response).to_not render_template(:news)
			end
		end		
	end

	context "GET edit" do
		it 'assigns task' do
			get :edit, id: @task
			expect(assigns(:task)).to eq(@task)
		end

		it 'render edit page' do
			get :edit, id: @task
			expect(response).to render_template('edit')
		end
	end

	context "POST create" do
		context "valid attributes" do
			it 'should create a new Task' do
				expect{
					post :create, task: attributes_for(:task) 
				}.to change(Task,:count).by(1)
			end

			it "redirects to the new task" do
				post :create, task: attributes_for(:task) 
				expect(response).to redirect_to Task.last
			end
		end

		context "invalid attributes" do
			it "should not create a new Task" do
				expect{
					post :create, task: attributes_for(:invalid_task) 
				}.to_not change(Task,:count)
			end

			it "re-renders the new method" do
				post :create, task: attributes_for(:invalid_task) 
				 render_template :new
			end
		end
	end

	context 'PUT update' do
		context "valid attributes" do
			it "changes task attributes" do 
				put :update, id: @task, task: attributes_for(:task, name: "Cool name")
				@task.reload
				expect(@task.name).to eql("Cool name")
			end

			it "redirects to the updated task" do
				put :update, id: @task, task: attributes_for(:task)
				expect(response).to redirect_to @task 
			end 
		end 

		context "invalid attributes" do
			it "does not change task attributes" do
				put :update, id: @task, task: attributes_for(:task, name: "Cool name", description: nil) 
				@task.reload
				expect(@task.name).to_not eq("Cool name")
			end 

			it "re-renders the edit method" do 
				put :update, id: @task, task: attributes_for(:invalid_task) 
				expect(response).to render_template :edit 
			end 
		end 
	end

	context "DELETE destroy" do
		it "deletes task" do
			expect{
				delete :destroy, id: @task
			}.to change(Task,:count).by(-1)
		end

		it "redirects to tasks_url" do
			delete :destroy, id: @task
			expect(response).to redirect_to tasks_url
		end
	end

	context "POST create_shared" do
		before(:each) do
			@shared_user = create(:user)
		end

		context "valid attributes" do
			it 'should create a new SharedTask' do
				expect{
					post :create_shared, id: @task.id, create_shared: { email: @shared_user.email}
				}.to change(SharedTask,:count).by(1)
			end

			it "redirects to the tasks" do
				post :create_shared, id: @task.id, create_shared: { email: @shared_user.email}
				expect(response).to redirect_to tasks_url
			end
		end

		context "invalid attributes" do
			before(:each) do
				@shared_task = SharedTask.create(task_id: @task.id, user_id: @shared_user.id)
			end

			it "should not create a new SharedTask" do				
				expect{
					post :create_shared, id: @task.id, create_shared: { email: @shared_user.email}
				}.to_not change(SharedTask,:count)
			end

			it "re-renders the new method" do
				post :create_shared, id: @task.id, create_shared: { email: @shared_user.email} 
				render_template :share
			end
		end
	end
end